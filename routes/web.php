<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $order_num = mt_rand(11111,99999);
    return view('welcome',['order_num'=> $order_num]);
});
//支付包支付
Route::get('/aliyun','Test\TestController@aliyun');
Route::get('/alipaySuccess','Test\TestController@alipaySuccess');
Route::post('/alipayNotify','Test\TestController@alipayNotify');

//微信登陆
Route::get('/api/weixin','Test\TestController@weixin');
Route::get('/getcode','Test\TestController@getcode');