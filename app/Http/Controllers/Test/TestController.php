<?php

namespace App\Http\Controllers\Test;

use App\Jobs\ProcessPodcas;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function test()
    {
        $a = date('Y-m-d h:i:s',time());
        echo strtotime($a);
        echo  '邮箱发送成功';
        echo "<br>";
        echo  '队列开始';
        echo "<br>";
        $data =  'this is pig and is rand   '.mt_rand(1111,9999);
        //加入队列
        ProcessPodcas::dispatch($data)->onQueue('test')->delay(now()->addMinutes(1));

    }
    public function rsa1()
    {
        $data = [
            'name'=> '卢柳晴',
            'sex'=> '男',
            'age'=> '30',
            'text'=> 'this is Idiot',
             'hahaha'=> '傻der'
        ];
        $json = json_encode($data);

        //获取私钥
        $key = openssl_get_privatekey('file://'.storage_path('app/public/private.pem'));
        //encrypt
        openssl_private_encrypt($json,$crypted,$key,OPENSSL_PKCS1_PADDING);
        $b64 = base64_encode($crypted);
        $url = "http://review.com/rsa";
        //初始化 创建新资源
        $ch = curl_init();
        // 设置 URL 和相应的选项
        curl_setopt($ch, CURLOPT_URL, $url);
        //发送post请求
        curl_setopt($ch, CURLOPT_POST, 1);
//        //禁止浏览器输出
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //发送数据
        curl_setopt($ch, CURLOPT_POSTFIELDS, $b64);
        //字符串文本
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type:text/plain']);
        //抓取 URL 并把它传递给浏览器
        $rs = curl_exec($ch);  //data 数据
        var_dump($rs);
        // 错误码
//        var_dump(curl_error($ch));
        // 关闭 cURL 资源，并且释放系统资源
        curl_close($ch);

    }
       /*
        * 支付宝支付
        * 访问地址 ： http://review.com/aliyun
        * 方式 get
        * */
    public function aliyun(Request $request)
    {
        $order_num = $request-> input('num');  //订单号
        if(!$order_num)
        {
            respones('no','无此订单');
        }
        $aliyun_url = "https://openapi.alipaydev.com/gateway.do";//支付宝网关
        $user_agent  = json_encode($_SERVER['HTTP_USER_AGENT']);
        $str = 'Windows';
        if(strpos($user_agent,$str) !=false)
        {
            //（pc端）扫码支付
            $method = 'alipay.trade.page.pay';  //接口名称
            $prouct_code = 'FAST_INSTANT_TRADE_PAY'; //产品码
        }else{
            //h5 （移动端）手机支付
            $method = 'alipay.trade.wap.pay';
            $prouct_code = 'QUICK_WAP_WAY';
        }
        $total = mt_rand(1111,9999) / 10;
        //业务参数
        $bizcont = [
            'subject' => '不管你就是要付钱',//交易标题/订单标题/订单关键
            'out_trade_no'=>$order_num, //订单号
            'total_amount'  => $total, //支付金额
            'product_code' => $prouct_code //固定值
        ];
        //工共参数
        $info = [
            'app_id'=> '2016092700608889',
            'method'=> $method,
            'format'   => 'JSON',
            'charset'   => 'utf-8',
            'sign_type'   => 'RSA2',
            'timestamp'   => date('Y-m-d H:i:s'),
            'version'   => '1.0',
            'return_url'=> 'http://money.mneddx.com/alipaySuccess',//支付成功同步通知地址
            'notify_url'=> 'http://money.mneddx.com/alipayNotify',//支付成功异步通知地址
            'biz_content'=> json_encode($bizcont)
        ];
        //拼接参数
        ksort($info);
        $i  = '';   //?a=b&b=c
        foreach ($info as $k=> $v)
        {
            $i.=$k.'='.$v.'&';
        }
        $info_str = rtrim($i,'&');
        //获取私钥 生成签名
        $rsaPrivateKeyFilePath = openssl_get_privatekey('file://'.storage_path('app/keys/private.pem')); //密钥
        //生成签名
        openssl_sign($info_str,$sign,$rsaPrivateKeyFilePath,OPENSSL_ALGO_SHA256);
        $sign = base64_encode($sign);
        $info['sign'] = $sign;
        //拼接url ？a=b & b=c
        $a = '?';
        foreach($info as $key=>$val){
            $a.=$key.'='.urlencode($val).'&'; //urlencode 将字符串以url形式编码
        }
        $info_str2 = rtrim($a,'&');
        $url = $aliyun_url.$info_str2;
        header('refresh:2;url='.$url);
    }
    /*
     * 支付包同步支付成功授权回调
     * get
     * */
    public  function alipaySuccess()
    {
        echo '恭喜你个臭der 支付成功：三秒后跳至首页';
        header('refresh:3;url=http://money.mneddx.com/');
    }
    /*
     * 支付宝支付异步通知
     * post
     * */
    public function alipayNotify()
    {
        //TODO 验签 更新订单状态  写入日志
        $rsaPrivateKeyFilePath = openssl_get_publickey('file://'.storage_path('app/keys/public.pem')); //公钥
        $a = openssl_verify($_POST,$_POST['sign'],$rsaPrivateKeyFilePath);

        $json_data = json_encode($_POST,JSON_UNESCAPED_UNICODE);
        //存入日志
        $log_str = "\n>>>>>> " .date('Y-m-d H:i:s') . ' '.$json_data .'this is 验签'.$a. " \n";
        is_dir('logs') or mkdir('logs',0777,true);
        file_put_contents('logs/alipay_notify',$log_str,FILE_APPEND);

        //更新订单
    }
    public function weixin()
    {
        $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".env('APPID')."&redirect_uri=".urlencode('http://money.mneddx.com/getcode')."&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect";
        header('refresh:0;url='.$url);
    }

    public function getcode()
    {
        $code = $_GET['code'];
        //获取accesstoken
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".env('APPID')."&secret=f583f90f3aed8ec33ae6dd30eceebe5f&code=".$code."&grant_type=authorization_code";
        $json_data=file_get_contents($url);
        $arr_info = json_decode($json_data,true);
        $access_token = $arr_info['access_token'];
        $openid = $arr_info['openid'];
        //获取用户信息
        $url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN';
        $user_info = json_decode(file_get_contents($url),true);
        echo '<pre>';print_r($user_info);echo "<pre>";
    }
}
